@extends('layouts.dashboard')

@section('content')
    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/bg.html')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>RESET PASSWORD</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>
    <!--Header section end-->


    <!--login section start-->
    <section  class="circle-section section-padding section-background">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-md-offset-3">
                    <div class="login-admin login-admin1">
                        <div class="login-header text-center">
                            <h6>RESET PASSWORD</h6>
                        </div>


                        <div class="login-form">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <input type="text" name="email" id="email" required placeholder="Enter your Email"/>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input type="password" name="password" id="password" required placeholder="Enter your Password"/>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input type="password" name="password_confirmation" id="password" required placeholder="Enter your Password"/>

                                <input value="Reset Password" type="submit">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
