<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Investment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function changePassword(Request $request) {

        $user = Auth::user();

        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;
        $newPasswordAgain = $request->newPasswordAgain;

        if (empty($oldPassword) || empty($newPassword) || empty($newPasswordAgain)) {
            return Redirect::back()->withErrors(['All Fields are required', 'The Message']);
        }

        if ($newPassword != $newPasswordAgain) {
            return Redirect::back()->withErrors(['New password and new password again does not match', 'The Message']);
        }

        if (Hash::check($oldPassword, $user->password)) {
            $user->password = Hash::make($newPassword);
            $user->save();

            return redirect()->back()->with('success', 'Password Changed Successfully');
        } else {
            return Redirect::back()->withErrors(['Incorrect old password passed', 'The Message']);
        }

    }

    public function test() {
        $investments =  Investment::with('user')->get();
        foreach ($investments as $investment) {
            $timesToPay = $investment->repeat_time;
            $timesPaid = $investment->paid_time;

            if ($timesPaid < $timesToPay) {
                $user = User::find($investment->user_id);

                if ($timesToPay == $timesPaid + 1) {
                    $user->wallet = $user->wallet + $investment->roi + $investment->amount_invested;
                } else {
                    $user->wallet = $user->wallet + $investment->roi;
                }
                $user->save();
                $investment->paid_time = $investment->paid_time + 1;
                $investment->save();

                echo 'ok';
            } else {
                echo 'bad';
            }
        }

    }
}
