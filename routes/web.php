<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/sign-in', function () {
    return view('login');
})->name('sign-in');

Route::get('/sign-up', function () {
    return view('register');
});

Route::get('/password-reset', function () {
    return view('password-reset');
});


Auth::routes();

// User Routes
Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/new-investment', 'InvestmentController@index')->name('investment');
Route::get('/fund-wallet', 'HomeController@fundWallet')->name('fund-wallet');
Route::post('/submit-payment-proof', 'AdminController@postPaymentProof');
Route::post('/log-investment', 'InvestmentController@logInvestment');
Route::get('/withdraw-funds', 'HomeController@withdrawFunds');
Route::post('/process-withdrawal', 'InvestmentController@processWithdrawal');
Route::get('/withdrawal-history', 'HomeController@withdrawalHistory');
Route::get('/deposit-history', 'HomeController@depositHistory');
Route::get('/change-password', 'HomeController@changePassword');
Route::post('/change-password', 'UserController@changePassword');
Route::get('/test', 'UserController@test');
Route::get('/investment-history', 'HomeController@investmentHistory');
Route::get('/roi-history', 'HomeController@roiHistory');
Route::get('/transaction-log', 'HomeController@transactionLog');

// Admin Routes
Route::get('/admin-dashboard', 'AdminController@index')->middleware('admin');
Route::get('/account-info', 'AdminController@settings')->middleware('admin');
Route::post('/settings', 'AdminController@updateSettings')->middleware('admin');
Route::get('/payment-proofs', 'AdminController@allPayments')->middleware('admin');
Route::get('/update-proof/{id}', 'AdminController@updateProof')->middleware('admin');
Route::get('/update-withdrawal/{id}', 'AdminController@updateWithdrawal')->middleware('admin');
Route::post('/credit-wallet', 'AdminController@creditWallet')->middleware('admin');
Route::get('/withdrawal-requests', 'AdminController@withdrawalRequest')->middleware('admin');
Route::get('/delete-account/{id}', 'AdminController@deleteUserAccount')->middleware('admin');
