<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bitcryptopay | Revolutionary Money Making Platform - Home Page</title>
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href="assets/images/logo/icon.png">
    <!--bootstrap Css-->
    <link href="assets/front/css/bootstrap.min.css" rel="stylesheet">
    <!--font-awesome Css-->
    <link href="assets/front/css/font-awesome.min.css" rel="stylesheet">
    <!-- Lightcase  Css-->
    <link href="assets/front/css/lightcase.css" rel="stylesheet">
    <!--WOW Css-->
    <link href="assets/front/css/animate.min.css" rel="stylesheet">
    <!--Slick Slider Css-->
    <link href="assets/front/css/slick.css" rel="stylesheet">
    <!--Slick Nav Css-->
    <link href="assets/front/css/slicknav.min.css" rel="stylesheet">
    <!--Swiper  Css-->
    <link href="assets/front/css/swiper.min.css" rel="stylesheet">
    <!--Style Css-->
    <link href="assets/front/css/style.css" rel="stylesheet">
    <!-- Theam Color Css-->
    <link href="assets/css/color82c6.css?color=0084D4" rel="stylesheet">
    <!--Responsive Css-->
    <link href="assets/front/css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="assets/css/ranger-style.css">
    <link rel="stylesheet" href="assets/css/ion.rangeSlider.skinFlat.css">
    <style>
        .price-table {
            margin-bottom: 45px;
    </style>
    <script src="assets/js/jquery.min.js"></script>

    <link rel="stylesheet" href="assets/front/2/css/style.css">

    <script src="assets/front/2/js/modernizr.js"></script>

    <link rel="stylesheet" type="text/css" href="../cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <script src="../cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="assets/js/countries.js"></script>
</head>

<body data-spy="scroll">
<!-- Start Pre-Loader-->
<div id="preloader">
    <div data-loader="circle-side"></div>
</div>

<!--main menu section start-->
@include('includes.admin-auth-navbar')
<!--main menu section end-->

@yield('content')

<div class="clearfix"></div>


<div class="clearfix"></div>


<!--footer area start-->
<style type="text/css">
    li.export-main {
        visibility: hidden;
    }
</style>
<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<!--jquery script load-->
<script src="assets/front/js/jquery.js"></script>

<script src="assets/front/js/bootstrap.min.js"></script>
<!-- Gmap Load Here -->
<script src="assets/front/js/gmaps.js"></script>
<!-- Map Js File Load -->
<script src="assets/front/js/map-script82c6.php?color=0084D4"></script>
<!-- Highlight script load-->
<script src="assets/front/js/highlight.min.js"></script>
<!--Jquery Ui Slider script load-->
<script src="assets/front/js/jquery-ui-slider.min.js"></script>
<!--Circleful Js File Load-->
<script src="assets/front/js/jquery.circliful.js"></script>
<!--CounterUp script load-->
<script src="assets/front/js/jquery.counterup.min.js"></script>
<!-- Ripples  script load-->
<script src="assets/front/js/jquery.ripples-min.js"></script>
<!--Slick Nav Js File Load-->
<script src="assets/front/js/jquery.slicknav.min.js"></script>
<!--Lightcase Js File Load-->
<script src="assets/front/js/lightcase.js"></script>
<!--particle Js File Load-->
<script src="assets/front/js/particles.min.js"></script>
<!--particle custom Js File Load-->
<script src="assets/front/js/particles-custom.js"></script>
<!--RainDrops script load-->
<script src="assets/front/js/raindrops.js"></script>
<!--Easing script load-->
<script src="assets/front/js/easing-min.js"></script>
<!--Slick Slider Js File Load-->
<script src="assets/front/js/slick.min.js"></script>
<!--Swiper script load-->
<script src="assets/front/js/swiper.min.js"></script>
<!--WOW script load-->
<script src="assets/front/js/wow.min.js"></script>
<!--WayPoints script load-->
<script src="assets/front/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/ion.rangeSlider.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true
        });
        wow.init();
    });
</script>
<script>
    $.each($('.slider-input'), function () {
        var $t = $(this),

            from = $t.data('from'),
            to = $t.data('to'),

            $dailyProfit = $($t.data('dailyprofit')),
            $totalProfit = $($t.data('totalprofit')),

            $val = $($t.data('valuetag')),

            perDay = $t.data('perday'),
            perYear = $t.data('peryear');


        $t.ionRangeSlider({
            input_values_separator: ";",
            prefix: '$ ',
            hide_min_max: true,
            force_edges: true,
            onChange: function (val) {
                $val.val('$ ' + val.from);

                var profit = (val.from * perDay / 100).toFixed(1);
                profit = '$ ' + profit.replace('.', '.');
                $dailyProfit.text(profit);

                profit = ((val.from * perDay / 100) * perYear).toFixed(1);
                profit = '$ ' + profit.replace('.', '.');
                $totalProfit.text(profit);

            }
        });
    });
    $('.invest-type__profit--val').on('change', function (e) {

        var slider = $($(this).data('slider')).data("ionRangeSlider");

        slider.update({
            from: $(this).val().replace('$ ', "")
        });
    })
</script>
<!--Main js file load-->
<script src="assets/front/js/main.js"></script>
<script language="javascript">
    populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
    populateCountries("country2");
    populateCountries("country2");
</script>
<script src="assets/front/2/js/main.js"></script>
<!-- <script src="https://bitcryptopay.com/assets/js/main.js"></script> -->
<!--swal alert message-->

<!--end swal alert message-->
<script>
    var mobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

    hljs.initHighlightingOnLoad();
    hljs.configure({useBR: true});
    jQuery('#raindrops').raindrops({color: '#fff', canvasHeight: 5});
    jQuery('#raindrops-green').raindrops({color: '#0084D4 ', canvasHeight: 5});

</script>

</body>
</html>
