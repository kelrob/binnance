@extends('layouts.admin-auth-dashboard')

@section('content')

    <div class="container">
        <section style="margin-top: 4%;">
            @if (session('message'))
                <div class="alert alert-success">
                    <p>{{ session('message') }}</p>
                </div>
            @endif
            <form method="post" action="{{ url('/settings') }}" style="padding: 2%; border: 1px solid lavender;" >
                @csrf
                <div class="form-group">
                    <label>Account to pay into</label>
                    <textarea class="form-control" name="account_info" style="resize: none;" rows="3">{{ $accountInfo }}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>
            </form>
        </section>
    </div>

@endsection
