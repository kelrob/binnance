@extends('layouts.auth-dashboard')

@section('content')
    <section class="breadcrumb-section"
             style="background-image: url('https://bitcryptopay.com/assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>Transaction Log</h5>
                    </div>
                    <!-- Breadcrumb section End -->

                </div>
            </div>
        </div>
    </section>
    <section class="trading-view">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript"
                    src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                const data = {
                    "symbols": [
                        {
                            "title": "S&P 500",
                            "proName": "OANDA:SPX500USD"
                        },
                        {
                            "title": "Shanghai Composite",
                            "proName": "INDEX:XLY0"
                        },
                        {
                            "title": "EUR/USD",
                            "proName": "FX_IDC:EURUSD"
                        },
                        {
                            "title": "BTC/USD",
                            "proName": "BITSTAMP:BTCUSD"
                        },
                        {
                            "title": "ETH/USD",
                            "proName": "BITSTAMP:ETHUSD"
                        }
                    ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "displayMode": "adaptive",
                    "locale": "en"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </section>

    <div class="content_padding">
        <div class="container user-dashboard-body">
            <div class="row" style="margin-top: 4%;">
                <div class="col-md-12">
                </div>
            </div><!---ROW-->

            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center">Withdrawal Transactions</h3>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="table">
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <td>Amount</td>
                            <td>Status</td>
                            <td>Date</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($withdrawals as $withdraw)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $withdraw->amount }}</td>
                                <td>
                                    @if ($withdraw->attended_to == 1)
                                        <span class="badge badge-success" style="background-color: green">Attended to</span>
                                    @elseif ($withdraw->attended_to == 0)
                                        <span class="badge badge-danger" style="background-color: #f85d51">Not Attended to</span>
                                    @endif
                                </td>
                                <td>{{ $withdraw->created_at->format('D d M Y') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <p style="margin-top: 8%"></p>

            <div class="row mt-4" style="margin-top: 7%">
                <div class="col-lg-12">
                    <h3 class="text-center">Deposits Transactions</h3>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="table">
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <td>Amount</td>
                            <td>Status</td>
                            <td>Date</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($deposits as $deposit)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $deposit->amount }}</td>
                                <td>
                                    @if ($deposit->seen == 1)
                                        <span class="badge badge-success" style="background-color: green">Attended to</span>
                                    @elseif ($deposit->seen == 0)
                                        <span class="badge badge-danger" style="background-color: #f85d51">Not Attended to</span>
                                    @endif
                                </td>
                                <td>{{ $deposit->created_at->format('D d M Y') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="invest_review_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text-center">You are about to invest on the <b><span id="invest"></span></b> Package</h4>
                            <hr />
                        </div>
                        <div id="response" style="margin-top: 4%;"></div>
                        <div id="hide">
                            <div class="col-lg-12">
                                <form autocomplete="off" method="post">
                                    @csrf
                                    <div id="response"></div>
                                    <div class="form-group">
                                        <label for="amount">Enter Amount</label>
                                        <input type="number" name="amount" id="amount" class="form-control"/>
                                    </div>
                                </form>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <a class="btn btn-primary btn-sm pull-right" id="proceed" onclick="logInvestment()">Proceed</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                                <a class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
