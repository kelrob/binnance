@extends('layouts.admin-auth-dashboard')

@section('content')

    <div class="container">
        <section style="margin-top: 4%;">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                <table class="table table-striped table-hover" id="myTable">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Full Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Total ROI</th>
                        <th>Account Type</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->investment->sum('roi') }}</td>
                            <td>{{ ucfirst($user->account_type) }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <a href="{{ url('/delete-account/' . $user->id) }}" class="btn btn-danger">Delete Account</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {{--        <h1 class="text-center">Welcome Admin {{ Auth::user()->username }}</h1>--}}
            {{--        <p class="text-center">Content will come here soon. Check the settings menu for now</p>--}}
            {{--        <p class="text-center"><a href="#" class="btn btn-primary">Get Started</a> </p>--}}
        </section>
    </div>

    <script>
        function myFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>

@endsection
