@extends('layouts.dashboard')

@section('content')
    <!--Header section start-->
    <section id="particles-js" class="header-area header-bg"
             style="background-image: url('assets/images/slider/5e9c6c96b55dc.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="header-section-wrapper">
                        <div class="header-section-top-part">
                            <div class="text-first wow slideInLeft" data-wow-duration="2s"><h1
                                    style="margin-top: 0px; margin-bottom: 15px; font-size: 38px; font-family: Montserrat, sans-serif; text-align: center;">
                                    <br></h1>
                                <h1 style="margin-top: 0px; margin-bottom: 15px; font-size: 38px; font-family: Montserrat, sans-serif; text-align: center;">
                                    Revolutionary Money Making Platform
                                    <div style=""><span style="font-size: 40px; font-weight: 800;"><font color="#0284D4">Binance Minning </font></span><br>
                                        <script src="../widgets.coingecko.com/coingecko-coin-converter-widget.js"></script>
                                        <coingecko-coin-converter-widget coin-id="bitcoin" currency="usd"
                                                                         background-color="" font-color="#0284D4"
                                                                         locale="en"></coingecko-coin-converter-widget>
                                        </span></div>
                                </h1>
                            </div>
                            <p style="font-size: 1.5em;" class=" wow slideInDown" data-wow-duration="2s"><br></p>
                        </div>
                        <div class="header-section-bottom-part">
                            <div class="domain-search-from">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Header section end-->
    <div class="clearfix"></div>
    <!-- Admin section start -->
    <div class="admin-section wow slideInRight" data-wow-duration="2s">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- admin content start -->
                    <div class="admin-content">
                        <!-- admin text start -->
                        <div class="admin-text">
                            <p>Get access to Your account</p>
                        </div>
                        <!-- admin text end -->
                        <!-- admin user start -->
                        <div class="admin-user">
                            <a href="login.html">
                                <button type="submit" name="login">sign in</button>
                            </a>
                            <a href="register.html">
                                <button type="submit" name="register">register now</button>
                            </a>
                        </div>
                        <!-- admin user end -->
                    </div>
                    <!-- admin-content end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Admin section end -->

    <div class="clearfix"></div>
    <!-- Circle Section Start -->
    <section class="circle-section section-padding wow slideInUp" data-wow-duration="2s">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header">
                        <h2>HOW <span> Binance Minning </span> Works </h2>
                        <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="circle-item wow flipInY" data-wow-duration="2s">
                        <img src="assets/images/features/5e9c5ef83ba38.png" width="50%" alt="items">
                        <div class="circle-content">
                            <h6>Security First</h6>
                            <p>Security will always be our top consideration. Our platform was built with multiple layers of
                                protection, deploying the most effective and reliable technologies to keep funds and
                                transactions secure.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="circle-item wow flipInY" data-wow-duration="2s">
                        <img src="assets/images/features/5e9c5f0751feb.png" width="50%" alt="items">
                        <div class="circle-content">
                            <h6>Total Innovation</h6>
                            <p>We believe in the potential of blockchain to provide groundbreaking solutions across
                                industries and beyond crypto. We are working with teams around the world to advance new,
                                inventive tokens that can transform the way goods, services and operations are managed
                                everywhere.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="circle-item wow flipInY" data-wow-duration="2s">
                        <img src="assets/images/features/5e9c5fe6e7848.png" width="50%" alt="items">
                        <div class="circle-content">
                            <h6>Robust Trading</h6>
                            <p>Our trading engine was custom-built for scale and speed to facilitate real-time order
                                execution under heavy demand. We support third-party trading platforms and algorithmic
                                trading via our extensive APIs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Circle Section End -->
    <div class="clearfix"></div>

    <!--About community Section Start-->
    <section class="section-padding sale-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <div class="sale-header wow slideInDown" data-wow-duration="2s">
                            <h2>about <span> Binance Minning </span></h2>
                        </div>
                        <div class="sale-content">
                            <div class="row">
                                <div class="col-md-6 wow slideInLeft" data-wow-duration="2s">
                                    <p class="about-community-text">
                                        <iframe width="375" height="380" src="https://www.youtube.com/embed/Um63OQz3bjo"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </p>
                                </div>
                                <div class="col-md-6 wow slideInRight" data-wow-duration="2s">
                                    <p class="about-community-text text-justify">
                                    <div><span style="color: rgb(255, 255, 255); font-size: large;">Take comfort knowing that we already have millions of customers across Binance Minning, our multi-currency, one of the world’s most investment, active crypto exchanges. Plus, we require sign in and make your privacy our priority. </span><br>
                                    </div>
                                    <div><font color="#ffffff" size="4">
                                            The company opened its doors to the public as a cryptocurrency investment
                                            company, offering one of the most comprehensive opportunities for Bitcoin
                                            enthusiasts to invest in the future of the world’s most famous
                                            cryptocurrency.</font></div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--About community Section end-->
    <div class="clearfix"></div>
    <!--service section start-->
    <section class="service-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center section-padding padding-bottom-0 wow slideInLeft"
                         data-wow-duration="2s">
                        <div class="section-header">
                            <h2>Our <span>Services</span></h2>
                            <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                        </div>
                        <p>When everyone participates, everything changes. Whether you’re building financial freedom,
                            generating wealth for clients, developing decentralized apps or growing your own company,
                            Bitcryptopay can help.</p>
                    </div>
                </div>
            </div>
            <div class="row wow slideInRight" data-wow-duration="2s">
                <div class="col-md-3 col-sm-6">
                    <div class="service-wrapper text-center">
                        <div class="service-icon ">
                            <i class="fa fa-rocket" aria-hidden="true"></i>
                        </div>
                        <div class="service-title">
                            <p>INVESTMENT</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-wrapper text-center">
                        <div class="service-icon ">
                            <i class="fa fa-money" aria-hidden="true"></i>
                        </div>
                        <div class="service-title">
                            <p>PROFIT</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-wrapper text-center">
                        <div class="service-icon ">
                            <i class="fa fa-money" aria-hidden="true"></i>
                        </div>
                        <div class="service-title">
                            <p>WITHDRAW</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-wrapper text-center">
                        <div class="service-icon ">
                            <i class="fa fa-th" aria-hidden="true"></i>
                        </div>
                        <div class="service-title">
                            <p>PLANS</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--service section end-->

    <!--start investment plan-->
    <section class="section-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center section-padding padding-bottom-0 wow slideInLeft"
                         data-wow-duration="2s">
                        <div class="section-header">
                            <h2>Investment <span> Calculator</span></h2>
                            <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                        </div>
                        <p>Calculate your investment returns  (profit) — Your investment funds is automatically divided
                            across all coins in that bundle based on market cap (a coin’s price X the total number in
                            circulation).</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6 pricing-list-botom-margin wow zoomIn" data-wow-duration="3s">
                    <!-- Pricing  List1 Start -->
                    <div class="pricing-list1">
                        <div class="pricing-header1">
                            <h5>Starter</h5>
                            <p> 5% Daily for 4 days</p>
                        </div>
                        <div class="pricing-info1">
                            <ul>
                                <li><p>for <span class="color-text">4</span> days</p></li>
                                <li><p><span class="color-text">5%</span> ROI each day</p></li>
                            </ul>
                        </div>
                        <div class="price-range">
                            <div class="row">
                                <div class="col-md-6 text-left col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Minimum<span class="color-text">$ 200</span></h6>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Maximum<span class="color-text">$ 4000</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="invest-type__profit plan__value--1">
                            <input type="text" value="$ 2100" class="custom-input invest-type__profit--val"
                                   data-slider=".slider-input--1" style="color:#FFF; font-size: 25px">
                            <input type="hidden" name="amount" value="2100" class=" slider-input slider-input--1"
                                   data-perday="5" data-peryear="4" data-min="200" data-max="4000"
                                   data-dailyprofit=".daily-profit-1" data-totalprofit=".total-profit-1 "
                                   data-valuetag=".plan__value--1 .invest-type__profit--val"/>
                        </div>
                        <input type="hidden" name="plan_id" value="1">
                        <div class="price-range">
                            <div class="row">
                                <div
                                    class="col-md-6 text-left col-sm-6 col-xs-6 invest-type__calc invest-type__calc--daily">
                                    <div class="min-price">
                                        <h6>per day<span class="color-text"><b class="daily-profit-1">$ 105.0</b></span>
                                        </h6>
                                    </div>
                                </div>
                                <div
                                    class="col-md-6 text-right col-sm-6 col-xs-6 invest-type__calc invest-type__calc--total">
                                    <div class="min-price">
                                        <h6>Total Return<span class="color-text"><b
                                                    class="total-profit-1">$ 420.0</b></span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<a href="pricing-list.html">Order Now!</a>-->
                    </div>
                    <!-- Pricing List1 End -->
                </div>
                <div class="col-md-3 col-sm-6 pricing-list-botom-margin wow zoomIn" data-wow-duration="3s">
                    <!-- Pricing  List1 Start -->
                    <div class="pricing-list1">
                        <div class="pricing-header1">
                            <h5>Silver</h5>
                            <p> 10% Daily for 7 days</p>
                        </div>
                        <div class="pricing-info1">
                            <ul>
                                <li><p>for <span class="color-text">7</span> days</p></li>
                                <li><p><span class="color-text">10%</span> ROI each day</p></li>
                            </ul>
                        </div>
                        <div class="price-range">
                            <div class="row">
                                <div class="col-md-6 text-left col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Minimum<span class="color-text">$ 300</span></h6>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Maximum<span class="color-text">$ 9000</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="invest-type__profit plan__value--2">
                            <input type="text" value="$ 4650" class="custom-input invest-type__profit--val"
                                   data-slider=".slider-input--2" style="color:#FFF; font-size: 25px">
                            <input type="hidden" name="amount" value="4650" class=" slider-input slider-input--2"
                                   data-perday="10" data-peryear="7" data-min="300" data-max="9000"
                                   data-dailyprofit=".daily-profit-2" data-totalprofit=".total-profit-2 "
                                   data-valuetag=".plan__value--2 .invest-type__profit--val"/>
                        </div>
                        <input type="hidden" name="plan_id" value="2">
                        <div class="price-range">
                            <div class="row">
                                <div
                                    class="col-md-6 text-left col-sm-6 col-xs-6 invest-type__calc invest-type__calc--daily">
                                    <div class="min-price">
                                        <h6>per day<span class="color-text"><b class="daily-profit-2">$ 465.0</b></span>
                                        </h6>
                                    </div>
                                </div>
                                <div
                                    class="col-md-6 text-right col-sm-6 col-xs-6 invest-type__calc invest-type__calc--total">
                                    <div class="min-price">
                                        <h6>Total Return<span class="color-text"><b
                                                    class="total-profit-2">$ 3255.0</b></span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<a href="pricing-list.html">Order Now!</a>-->
                    </div>
                    <!-- Pricing List1 End -->
                </div>
                <div class="col-md-3 col-sm-6 pricing-list-botom-margin wow zoomIn" data-wow-duration="3s">
                    <!-- Pricing  List1 Start -->
                    <div class="pricing-list1">
                        <div class="pricing-header1">
                            <h5>Gold</h5>
                            <p> 20% Daily for 7 days</p>
                        </div>
                        <div class="pricing-info1">
                            <ul>
                                <li><p>for <span class="color-text">7</span> days</p></li>
                                <li><p><span class="color-text">20%</span> ROI each day</p></li>
                            </ul>
                        </div>
                        <div class="price-range">
                            <div class="row">
                                <div class="col-md-6 text-left col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Minimum<span class="color-text">$ 1000</span></h6>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Maximum<span class="color-text">$ 24000</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="invest-type__profit plan__value--3">
                            <input type="text" value="$ 12500" class="custom-input invest-type__profit--val"
                                   data-slider=".slider-input--3" style="color:#FFF; font-size: 25px">
                            <input type="hidden" name="amount" value="12500" class=" slider-input slider-input--3"
                                   data-perday="20" data-peryear="7" data-min="1000" data-max="24000"
                                   data-dailyprofit=".daily-profit-3" data-totalprofit=".total-profit-3 "
                                   data-valuetag=".plan__value--3 .invest-type__profit--val"/>
                        </div>
                        <input type="hidden" name="plan_id" value="3">
                        <div class="price-range">
                            <div class="row">
                                <div
                                    class="col-md-6 text-left col-sm-6 col-xs-6 invest-type__calc invest-type__calc--daily">
                                    <div class="min-price">
                                        <h6>per day<span class="color-text"><b class="daily-profit-3">$ 2500.0</b></span>
                                        </h6>
                                    </div>
                                </div>
                                <div
                                    class="col-md-6 text-right col-sm-6 col-xs-6 invest-type__calc invest-type__calc--total">
                                    <div class="min-price">
                                        <h6>Total Return<span class="color-text"><b
                                                    class="total-profit-3">$ 17500.0</b></span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<a href="pricing-list.html">Order Now!</a>-->
                    </div>
                    <!-- Pricing List1 End -->
                </div>
                <div class="col-md-3 col-sm-6 pricing-list-botom-margin wow zoomIn" data-wow-duration="3s">
                    <!-- Pricing  List1 Start -->
                    <div class="pricing-list1">
                        <div class="pricing-header1">
                            <h5>Platinum</h5>
                            <p> 40% Daily for 14 days</p>
                        </div>
                        <div class="pricing-info1">
                            <ul>
                                <li><p>for <span class="color-text">14</span> days</p></li>
                                <li><p><span class="color-text">40%</span> ROI each day</p></li>
                            </ul>
                        </div>
                        <div class="price-range">
                            <div class="row">
                                <div class="col-md-6 text-left col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Minimum<span class="color-text">$ 5000</span></h6>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right col-sm-6 col-xs-6">
                                    <div class="min-price">
                                        <h6>Maximum<span class="color-text">$ 90000</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="invest-type__profit plan__value--4">
                            <input type="text" value="$ 47500" class="custom-input invest-type__profit--val"
                                   data-slider=".slider-input--4" style="color:#FFF; font-size: 25px">
                            <input type="hidden" name="amount" value="47500" class=" slider-input slider-input--4"
                                   data-perday="40" data-peryear="14" data-min="5000" data-max="90000"
                                   data-dailyprofit=".daily-profit-4" data-totalprofit=".total-profit-4 "
                                   data-valuetag=".plan__value--4 .invest-type__profit--val"/>
                        </div>
                        <input type="hidden" name="plan_id" value="4">
                        <div class="price-range">
                            <div class="row">
                                <div
                                    class="col-md-6 text-left col-sm-6 col-xs-6 invest-type__calc invest-type__calc--daily">
                                    <div class="min-price">
                                        <h6>per day<span class="color-text"><b class="daily-profit-4">$ 19000.0</b></span>
                                        </h6>
                                    </div>
                                </div>
                                <div
                                    class="col-md-6 text-right col-sm-6 col-xs-6 invest-type__calc invest-type__calc--total">
                                    <div class="min-price">
                                        <h6>Total Return<span class="color-text"><b
                                                    class="total-profit-4">$ 266000.0</b></span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<a href="pricing-list.html">Order Now!</a>-->
                    </div>
                    <!-- Pricing List1 End -->
                </div>
            </div>
            <div class="row section-padding padding-bottom-0">
                <div class="col-md-6 col-sm-6">
                    <div class="contact-middel-info wow bounceInLeft" data-wow-duration="2s">
                        <div class="contact-middel-title">
                            <h4>Have a question <span>we are here to help!</span></h4>
                        </div>
                        <div class="contact-middel-details">
                            <p><i class="fa fa-phone"></i> +1(224) 698-7620</p>
                            <p><i class="fa fa-envelope"></i> support@binanceminning.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="discunt-middel-text wow bounceInRight" data-wow-duration="2s">
                        <h3>3<i class="fa fa-percent"></i> <br/> referral <br/> commission</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end start investment plan-->
    <!--Our Top Investor Section Start-->
    <div class="clearfix"></div>
    <section class="commission-section section-padding ">
        <div class="container">
            <!-- section header start -->
            <div class="section-header wow slideInLeft" data-wow-duration="2s">
                <h2>Our top <span> Investors</span></h2>
                <p><img src="assets/images/logo/icon.png" alt="icon"></p>
            </div>
            <p class="margin-b-35 wow slideInRight" data-wow-duration="2s">Join our top investors and live large. We do the
                hard part while you rest and monitor your income.</p>
            <!-- section header end -->
            <div class="row">
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Jessica Angella</p>
                        <h4><span> $ </span>100,000</h4>
                        <h5>1</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Thomas Edward</p>
                        <h4><span> $ </span>50,000</h4>
                        <h5>2</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Nelson Mandiv</p>
                        <h4><span> $ </span>48,000</h4>
                        <h5>3</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Cuba Thompson</p>
                        <h4><span> $ </span>40,000</h4>
                        <h5>4</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Santo Royking</p>
                        <h4><span> $ </span>25,000</h4>
                        <h5>5</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Jessica Angella</p>
                        <h4><span> $ </span>20,000</h4>
                        <h5>6</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Thomas Edward</p>
                        <h4><span> $ </span>10,000</h4>
                        <h5>7</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="referral-amount wow zoomIn" data-wow-duration="3s">
                        <p>Jessica Angella</p>
                        <h4><span> $ </span>9,000</h4>
                        <h5>8</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Our Top Investor Section Start-->

    <!--testimonial section start-->
    <section class="people-say-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- section header start -->
                    <div class="section-header wow bounceInLeft" data-wow-duration="2s">
                        <h2>What People <span>Say</span></h2>
                        <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                    </div>
                    <!-- section header end -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="testimonial-area">
                        <div class="row">
                            <div class="col-lg-12  col-md-10 ">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-2">
                                        <div class="testimonial-image-slider slider-nav text-center">

                                            <div class="sin-testiImage wow rotateIn" data-wow-duration="2s">
                                                <img src="assets/images/1513499671.jpg" alt="slider">
                                            </div>

                                            <div class="sin-testiImage wow rotateIn" data-wow-duration="2s">
                                                <img src="assets/images/1513499727.jpg" alt="slider">
                                            </div>

                                            <div class="sin-testiImage wow rotateIn" data-wow-duration="2s">
                                                <img src="assets/images/1513499751.jpg" alt="slider">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="testimonial-text-slider slider-for text-center wow bounceInRight"
                                     data-wow-duration="2s">

                                    <div class="sin-testiText">
                                        <!-- people sat content list start -->
                                        <div class="people-say-content-list  ">
                                            <h4>Ketthy Ben</h4>

                                            <h6>Teacher</h6>
                                            <ul>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                            </ul>
                                            <p>
                                                <font color="#000000">OMG, Like Magic! I got my return profit.. In my bank,
                                                    woow this is real</font>
                                            </p>
                                        </div>
                                        <!-- people-say-content-list end -->
                                    </div>

                                    <div class="sin-testiText">
                                        <!-- people sat content list start -->
                                        <div class="people-say-content-list  ">
                                            <h4>Judge Brooks</h4>

                                            <h6>CEO of MinerEUiswas</h6>
                                            <ul>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                            </ul>
                                            <p>
                                                <font color="#4b565e">MinerEU is very happy to have EasyPay as our
                                                    trustworthy partner. We have already recommended EasyPay to thousands of
                                                    our existing customers who are happy and satisfied with their excellent
                                                    investment services and returns.</font><br>
                                            </p>
                                        </div>
                                        <!-- people-say-content-list end -->
                                    </div>

                                    <div class="sin-testiText">
                                        <!-- people sat content list start -->
                                        <div class="people-say-content-list  ">
                                            <h4>Angenila Jolly</h4>

                                            <h6>Architecture Engineer</h6>
                                            <ul>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                            </ul>
                                            <p>
                                                I just have to leave you guys a positive feedback on my investment. You
                                                changed my dragging finance situation. Thank you alot
                                            </p>
                                        </div>
                                        <!-- people-say-content-list end -->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- row -->
    </section><!--  section -->
    <!--testimonial section start-->
    <div class="clearfix"></div>
    <!--Deopsit and Payouts section start-->
    <section class="hosting-section hosting-section1  section-padding section-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- section header start -->
                    <div class="section-header wow bounceInDown" data-wow-duration="3s">
                        <h2>Latest <span> Deposits & Withdraw</span></h2>
                        <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                    </div>
                    <!-- section header end -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="section-wrapper wow bounceInLeft" data-wow-duration="2s">
                        <div class="deposit-title text-center">
                            <h4>Latest Deposits</h4>
                        </div>
                        <div class="hosting-content table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Currency</th>
                                    <th>Amount</th>
                                </tr>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>Elijah ogbonna</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$2077</strong></td>
                                </tr>
                                <tr>
                                    <td>Uwamahoro Doriane</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$200</strong></td>
                                </tr>
                                <tr>
                                    <td>Juma Emmanuel</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$1000</strong></td>
                                </tr>
                                <tr>
                                    <td>Majakathata</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$200</strong></td>
                                </tr>
                                <tr>
                                    <td>LAVIE</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$300</strong></td>
                                </tr>
                                <tr>
                                    <td>Maleeto</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$50</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- hosting content end -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-wrapper wow bounceInRight" data-wow-duration="2s">
                        <div class="deposit-title text-center">
                            <h4>Latest Withdraw</h4>
                        </div>
                        <div class="hosting-content table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Currency</th>
                                    <th>Amount</th>
                                </tr>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>Uwamahoro Doriane</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$5743</strong></td>
                                </tr>
                                <tr>
                                    <td>Elijah ogbonna</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$7150</strong></td>
                                </tr>
                                <tr>
                                    <td>Juma Emmanuel</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$2118</strong></td>
                                </tr>
                                <tr>
                                    <td>Juma Emmanuel</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$19159</strong></td>
                                </tr>
                                <tr>
                                    <td>Majakathata</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$500</strong></td>
                                </tr>
                                <tr>
                                    <td>Majakathata</td>
                                    <td><strong>USD</strong></td>
                                    <td><strong>$6000</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- hosting content end -->
                    </div>
                </div>
            </div><!-- row -->
        </div><!-- container -->
    </section>
    <!--Deopsit and Payouts Section End-->

    <!-- Online Section End -->
@endsection
