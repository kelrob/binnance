<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Investment;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class InvestmentController extends Controller
{
    public function index()
    {
        return view('user-dashboard.investment');
    }

    public function logInvestment(Request $request)
    {
        $user = Auth::user();
        $investment = new Investment();

        $amount = $request->amount;
        $investmentType = $request->investmentType;

        if (empty($amount)) {
            return '<div class="alert alert-danger">Amount can not be empty</div>';
        } else {
            if ($investmentType == 'starter') {
                if ($amount >= 200) {
                    if ($user->wallet > $amount) {
                        $investment->user_id = $user->id;
                        $investment->plan = 'starter';
                        $investment->commission = 5;
                        $investment->repeat_time = 4;
                        $investment->compound = 'daily';
                        $investment->amount_invested = $amount;
                        $investment->roi = $amount * (5 / 100);
                        $investment->save();

                        $newWallet = $user->wallet - $amount;
                        $user->wallet = $newWallet;
                        $user->save();

                        return 'Congratulations your investment has been logged';
                    } else {
                        return '<div class="alert alert-danger">Insufficient Amount in wallet</div>';
                    }
                } else {
                    return '<div class="alert alert-danger">Insufficient Amount for this Package</div>';
                }
            } else if ($investmentType == 'silver') {
                if ($amount >= 300) {
                    if ($user->wallet > $amount) {
                        $investment->commission = 10;
                        $investment->repeat_time = 7;
                        $investment->user_id = $user->id;
                        $investment->plan = 'silver';
                        $investment->compound = 'daily';
                        $investment->amount_invested = $amount;
                        $investment->roi = $amount * (10 / 100);

                        $newWallet = $user->wallet - $amount;
                        $user->wallet = $newWallet;
                        $user->save();

                        return 'Congratulations your investment has been logged';
                    } else {
                        return '<div class="alert alert-danger">Insufficient Amount in wallet</div>';
                    }
                } else {
                    return '<div class="alert alert-danger">Insufficient Amount for this Package</div>';
                }
            } else if ($investmentType == 'gold') {
                if ($amount >= 1000) {
                    if ($user->wallet > $amount) {
                        $investment->commission = 20;
                        $investment->repeat_time = 7;
                        $investment->user_id = $user->id;
                        $investment->plan = 'gold';
                        $investment->compound = 'daily';
                        $investment->amount_invested = $amount;
                        $investment->roi = $amount * (20 / 100);

                        $newWallet = $user->wallet - $amount;
                        $user->wallet = $newWallet;
                        $user->save();

                        return 'Congratulations your investment has been logged';
                    } else {
                        return '<div class="alert alert-danger">Insufficient Amount in wallet</div>';
                    }
                } else {
                    return '<div class="alert alert-danger">Insufficient Amount for this Package</div>';
                }
            } else if ($investmentType == 'platinum') {
                if ($amount >= 5000) {
                    if ($user->wallet > $amount) {
                        $investment->commission = 40;
                        $investment->repeat_time = 14;
                        $investment->user_id = $user->id;
                        $investment->plan = 'gold';
                        $investment->compound = 'daily';
                        $investment->amount_invested = $amount;
                        $investment->roi = $amount * (40 / 100);

                        $newWallet = $user->wallet - $amount;
                        $user->wallet = $newWallet;
                        $user->save();

                        return 'Congratulations your investment has been logged';
                    } else {
                        return '<div class="alert alert-danger">Insufficient Amount in wallet</div>';
                    }
                } else {
                    return '<div class="alert alert-danger">Insufficient Amount for this Package</div>';
                }
            } else {
                return '<div class="alert alert-danger">Invalid Package Selected</div>';
            }
        }
    }

    public function processWithdrawal(Request $request)
    {
        $userId = Auth::user()->id;
        $amount = $request->amount;
        $withdrawalMode = $request->withdrawl_mode;

        $pendingWithdrawal = WithdrawalRequest::where('user_id', $userId)
            ->where('attended_to', 0)
            ->count();

        if (empty($amount)) {
            return Redirect::back()->withErrors(['Amount can not be empty', 'The Message']);
        }

        if (Auth::user()->wallet < $amount) {
            return Redirect::back()->withErrors(['Insufficient amount in wallet', 'The Message']);
        }

        if ($withdrawalMode == 'bitcoin' && empty($request->bitcoin_wallet)) {
            return Redirect::back()->withErrors(['Bitcoin wallet must not be empty', 'The Message']);
        }

        if ($pendingWithdrawal == 0) {
            $withdrawal = new WithdrawalRequest();
            $withdrawal->user_id = $userId;
            $withdrawal->amount = $amount;
            $withdrawal->mode = $withdrawalMode;
            $withdrawal->bitcoin_wallet = $request->bitcoin_wallet;
            $withdrawal->save();

            return redirect()->back()->with('success', 'Withdrawal request placed successfully');
        } else {
            return Redirect::back()->withErrors(['You already have a pending withdrawal request that is yet to be attended to', 'The Message']);
        }
    }
}
