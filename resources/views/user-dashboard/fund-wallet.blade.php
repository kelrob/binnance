@extends('layouts.auth-dashboard')

@section('content')
    <div class="container">
        <section class="fund-wallet">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 3%;">
                        <h3 class="text-center">Fund your wallet</h3>
                        <hr style="width: 10%" />
                    </div>
                    <div class="col-lg-12">
                        <div style="background-color: #0084D4; padding: 2%; color: #fff; border-radius: 3px;" class="text-center text-white">
                            <h5 class="text-center" style="color: #fff">Kindly pay into the following account</h5>
                            <p>{!! $accountInfo  !!}</p>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 3%">
                        <p class="text-center font-weight-bold" style=""><a href="#" class="" onclick="toggleProofForm()">Click here if you have made payment</a></p>

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            <p class="text-center"><b>See Proof below</b></p>
                            <p class="text-center"><img src="uploads/{{ Session::get('file') }}"></p>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form style="padding: 1%; border: 1px solid #eee; margin-top: 4%; display: none;" id="toggleForm" method="post" action="{{ url('/submit-payment-proof') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input id="amount" type="number" name="amount" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Upload Proof of payment</label>
                                <input type="file" name="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        const toggleProofForm = () => {
            $('#toggleForm').fadeToggle();
        }
    </script>
@endsection
