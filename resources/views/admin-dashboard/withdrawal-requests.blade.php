@extends('layouts.admin-auth-dashboard')

@section('content')

    <div class="container">
        <section style="margin-top: 4%">
            <div class="">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>User</th>
                        <th>Email</th>
                        <th>Amount</th>
                        <th>Action</th>
                        <th>Mode</th>
                        <th>Details</th>
                        <th>Date Submitted</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($withdrawals as $withdrawal)
                        <tr>
                            <td>{{ $withdrawal->user->name }}</td>
                            <td>{{ $withdrawal->user->email }}</td>
                            <td>
                                {{ $withdrawal->amount }}
                            </td>
                            <td>
                                @if ($withdrawal->attended_to == 0)
                                    <a href="{{ url('/update-withdrawal/' . $withdrawal->id) }}" class="btn btn-xs btn-primary">Mark
                                        as attended</a>
                                @else
                                    <p>Attended to</p>
                                @endif
                            </td>
                            <td>{{ $withdrawal->mode }}</td>
                            <td>
                                @if($withdrawal->mode == 'bitcoin')
                                    {{ $withdrawal->bitcoin_wallet }}
                                @else
                                    {{ $withdrawal->user->bank_name }} <br />
                                    {{ $withdrawal->user->account_name }} <br />
                                    {{ $withdrawal->user->account_number }} <br />
                                @endif
                            </td>
                            <td>{{ $withdrawal->created_at->format('D d M Y') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination">
                    {{ $withdrawals->links() }}
                </div>
            </div>
        </section>

    </div>

@endsection
