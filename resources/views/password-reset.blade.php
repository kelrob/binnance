@extends('layouts.dashboard')

@section('content')
    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/bg.html')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>RESET PASSWORD</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>
    <!--Header section end-->


    <!--login section start-->
    <section  class="circle-section section-padding section-background">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-md-offset-3">
                    <div class="login-admin login-admin1">
                        <div class="login-header text-center">
                            <h6>RESET PASSWORD</h6>
                        </div>


                        <div class="login-form">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @if (count($errors->all()) > 0)
                                <div class="alert alert-danger" align="center">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br/>
                                    @endforeach
                                </div>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <input type="text" name="email" id="email" required placeholder="Enter your Email"/>

                                <input value="SEND PASSWORD RESET LINK" type="submit">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
