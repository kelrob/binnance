@extends('layouts.dashboard')

@section('content')
    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>Contact Page</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>

    <!--Contact Section-->
    <section class="contact-section contact-section1 section-padding section-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--Contact Info Tabs-->
                    <div class="contact-info">
                        <div class="row ">
                            <!-- contact-content Start -->
                            <div class="col-md-4">
                                <div class="contact-content">
                                    <div class="contact-header contact-form">
                                        <h2>Get In Touch</h2>
                                    </div>
                                    <div class="contact-list">
                                        <ul>
                                            <li>
                                                <div class="contact-thumb"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                                <div class="contact-text">
                                                    <p>Address:<span>FMC Tower at 30th and Walnut Streets</span></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="contact-thumb"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                                <div class="contact-text">
                                                    <p>Call Us :<span>+1(224) 698-7620</span></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="contact-thumb"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                                <div class="contact-text">
                                                    <p>Mail Us :<span>support@binanceminning.com</span></p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- contact-content End -->
                            <!--Form Column-->
                            <div class="form-column col-md-8 col-sm-12 ">
                                <!-- Contact Form -->
                                <div class="contact-form ">
                                    <h2>Send Message Us</h2>


                                    <form action="https://bitcryptopay.com/contact" method="post">
                                        <input type="hidden" name="_token" value="0ZcRJTDfAkFijGFxKAUegSatuHz3VePSWbDiKe7z">
                                        <div class="row clearfix">
                                            <div class="col-md-6  col-xs-12 form-group">
                                                <input type="text" name="name" placeholder="Your Name*" required="">
                                            </div>

                                            <div class="col-md-6  col-xs-12 form-group">
                                                <input type="email" name="email" placeholder="Email Address*" required="">
                                            </div>

                                            <div class=" col-md-12   form-group">
                                                <textarea name="message" placeholder="Your Message..."></textarea>
                                            </div>

                                            <div class=" col-md-12 form-group">
                                                <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Message</button>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                                <!--End Comment Form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Contact Section-->
@endsection
