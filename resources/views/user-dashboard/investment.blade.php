@extends('layouts.auth-dashboard')

@section('content')
    <section class="breadcrumb-section"
             style="background-image: url('https://bitcryptopay.com/assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>User New Investment</h5>
                    </div>
                    <!-- Breadcrumb section End -->

                </div>
            </div>
        </div>
    </section>
    <section class="trading-view">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript"
                    src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                const data = {
                    "symbols": [
                        {
                            "title": "S&P 500",
                            "proName": "OANDA:SPX500USD"
                        },
                        {
                            "title": "Shanghai Composite",
                            "proName": "INDEX:XLY0"
                        },
                        {
                            "title": "EUR/USD",
                            "proName": "FX_IDC:EURUSD"
                        },
                        {
                            "title": "BTC/USD",
                            "proName": "BITSTAMP:BTCUSD"
                        },
                        {
                            "title": "ETH/USD",
                            "proName": "BITSTAMP:ETHUSD"
                        }
                    ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "displayMode": "adaptive",
                    "locale": "en"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </section>

    <div class="content_padding">
        <div class="container user-dashboard-body">
            <div class="row" style="margin-top: 4%;">
                <div class="col-md-12">
                    <div class="row new-investment-user">

                        <div class="col-sm-4 text-center">
                            <div class="panel panel-green panel-pricing">
                                <div class="panel-heading">
                                    <h3 style="font-size: 28px;"><b>Starter</b></h3>
                                </div>
                                <div style="font-size: 18px;padding: 18px;" class="panel-body text-center">
                                    <p><strong>200 USD - 4000 USD</strong></p>
                                </div>
                                <ul style='font-size: 15px;' class="list-group text-center bold">
                                    <li class="list-group-item"><i class="fa fa-check"></i> Commission - 5 <i class="fa fa-percent"></i> </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Repeat - 4 times </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Compound - <span class="aaaa">Daily</span></li>
                                </ul>
                                <div class="panel-footer" style="overflow: hidden">
                                    <div class="col-sm-12">
                                        <input type="hidden" id="investment_type" name="type" onclick="setInvestmentType('starter')" value="starter" />
                                        <button type="button" onclick="setInvestmentType('starter')" class="btn btn-primary bold uppercase btn-block btn-icon icon-left plan_id radious-zero" value="1" data-toggle="modal" data-target="#invest_review_modal">
                                            <i class="fa fa-send"></i> Invest Under This Package
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 text-center">
                            <div class="panel panel-green panel-pricing">
                                <div class="panel-heading">
                                    <h3 style="font-size: 28px;"><b>Silver</b></h3>
                                </div>
                                <div style="font-size: 18px;padding: 18px;" class="panel-body text-center">
                                    <p><strong>300 USD - 9000 USD</strong></p>
                                </div>
                                <ul style='font-size: 15px;' class="list-group text-center bold">
                                    <li class="list-group-item"><i class="fa fa-check"></i> Commission - 10 <i class="fa fa-percent"></i> </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Repeat - 7 times </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Compound - <span class="aaaa">Daily</span></li>
                                </ul>
                                <div class="panel-footer" style="overflow: hidden">
                                    <div class="col-sm-12">
                                        <input type="hidden" id="investment_type" name="type" value="silver" />
                                        <button type="button" onclick="setInvestmentType('silver')" class="btn btn-primary bold uppercase btn-block btn-icon icon-left plan_id radious-zero" value="2" data-toggle="modal" data-target="#invest_review_modal">
                                            <i class="fa fa-send"></i> Invest Under This Package
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 text-center">
                            <div class="panel panel-green panel-pricing">
                                <div class="panel-heading">
                                    <h3 style="font-size: 28px;"><b>Gold</b></h3>
                                </div>
                                <div style="font-size: 18px;padding: 18px;" class="panel-body text-center">
                                    <p><strong>1000 USD - 24000 USD</strong></p>
                                </div>
                                <ul style='font-size: 15px;' class="list-group text-center bold">
                                    <li class="list-group-item"><i class="fa fa-check"></i> Commission - 20 <i class="fa fa-percent"></i> </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Repeat - 7 times </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Compound - <span class="aaaa">Daily</span></li>
                                </ul>
                                <div class="panel-footer" style="overflow: hidden">
                                    <div class="col-sm-12">
                                        <input type="hidden" id="investment_type" name="type" value="gold" />
                                        <button type="button" onclick="setInvestmentType('gold')" class="btn btn-primary bold uppercase btn-block btn-icon icon-left plan_id radious-zero" value="2" data-toggle="modal" data-target="#invest_review_modal">
                                            <i class="fa fa-send"></i> Invest Under This Package
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 text-center">
                            <div class="panel panel-green panel-pricing">
                                <div class="panel-heading">
                                    <h3 style="font-size: 28px;"><b>Platinum</b></h3>
                                </div>
                                <div style="font-size: 18px;padding: 18px;" class="panel-body text-center">
                                    <p><strong>5000 USD - 90000 USD</strong></p>
                                </div>
                                <ul style='font-size: 15px;' class="list-group text-center bold">
                                    <li class="list-group-item"><i class="fa fa-check"></i> Commission - 40 <i class="fa fa-percent"></i> </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Repeat - 14 times </li>
                                    <li class="list-group-item"><i class="fa fa-check"></i> Compound - <span class="aaaa">Daily</span></li>
                                </ul>
                                <div class="panel-footer" style="overflow: hidden">
                                    <div class="col-sm-12">
                                        <input type="hidden" id="investment_type" name="type" value="platinum" />
                                        <button type="button" onclick="setInvestmentType('platinum')" class="btn btn-primary bold uppercase btn-block btn-icon icon-left plan_id radious-zero" value="2" data-toggle="modal" data-target="#invest_review_modal">
                                            <i class="fa fa-send"></i> Invest Under This Package
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!---ROW-->
        </div>
    </div>

    <div id="invest_review_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text-center">You are about to invest on the <b><span id="invest"></span></b> Package</h4>
                            <hr />
                        </div>
                        <div id="response" style="margin-top: 4%;"></div>
                        <div id="hide">
                            <div class="col-lg-12">
                                <form autocomplete="off" method="post">
                                    @csrf
                                    <div id="response"></div>
                                    <div class="form-group">
                                        <label for="amount">Enter Amount</label>
                                        <input type="number" name="amount" id="amount" class="form-control"/>
                                    </div>
                                </form>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <a class="btn btn-primary btn-sm pull-right" id="proceed" onclick="logInvestment()">Proceed</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                                <a class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        const setInvestmentType = (value) => {
            $("#invest").html(value);
        }

        const logInvestment = () => {
            let investmentType = $('#invest').text();
            let amount = $('#amount').val()

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: 'log-investment',
                data: {
                  investmentType,
                  amount
                },
                beforeSend: function () {
                    $('#proceed').attr('disabled', 'disabled').html('Please Wait...');
                    $('#response').hide();
                },
                success: function (data) {
                    if (data == 'Congratulations your investment has been logged') {
                        $('#response').html(`<div class="alert alert-success">${data}</div>`).show();
                        $('#hide').hide();
                    } else {
                        $('#response').html(data).show();
                        $('#proceed').removeAttr('disabled').html('Proceed');
                    }
                }
            })
        }
    </script>
@endsection
