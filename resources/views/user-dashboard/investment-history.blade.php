@extends('layouts.auth-dashboard')

@section('content')
    <section class="breadcrumb-section"
             style="background-image: url('https://bitcryptopay.com/assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>Investment History</h5>
                    </div>
                    <!-- Breadcrumb section End -->

                   </div>
            </div>
        </div>
    </section>
    <section class="trading-view">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript"
                    src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                const data = {
                    "symbols": [
                        {
                            "title": "S&P 500",
                            "proName": "OANDA:SPX500USD"
                        },
                        {
                            "title": "Shanghai Composite",
                            "proName": "INDEX:XLY0"
                        },
                        {
                            "title": "EUR/USD",
                            "proName": "FX_IDC:EURUSD"
                        },
                        {
                            "title": "BTC/USD",
                            "proName": "BITSTAMP:BTCUSD"
                        },
                        {
                            "title": "ETH/USD",
                            "proName": "BITSTAMP:ETHUSD"
                        }
                    ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "displayMode": "adaptive",
                    "locale": "en"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </section>

    <div class="content_padding">
        <div class="container user-dashboard-body">
            <div class="row" style="margin-top: 4%;">
                <div class="col-md-12">
                </div>
            </div><!---ROW-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Investment Type</th>
                                <th>Amount Investment</th>
                                <th>Commission</th>
                                <th>Date Invested</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($histories as $history)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $history->plan }}</td>
                                    <td>{{ $history->amount_invested }}</td>
                                    <td>{{ $history->commission }}%</td>
                                    <td>{{ $history->created_at->format('D d M Y') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="invest_review_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text-center">You are about to invest on the <b><span id="invest"></span></b> Package</h4>
                            <hr />
                        </div>
                        <div id="response" style="margin-top: 4%;"></div>
                        <div id="hide">
                            <div class="col-lg-12">
                                <form autocomplete="off" method="post">
                                    @csrf
                                    <div id="response"></div>
                                    <div class="form-group">
                                        <label for="amount">Enter Amount</label>
                                        <input type="number" name="amount" id="amount" class="form-control"/>
                                    </div>
                                </form>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <a class="btn btn-primary btn-sm pull-right" id="proceed" onclick="logInvestment()">Proceed</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                                <a class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
