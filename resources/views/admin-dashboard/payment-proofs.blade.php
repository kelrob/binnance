@extends('layouts.admin-auth-dashboard')

@section('content')

    <div class="container">
       <section style="margin-top: 4%">
           <div class="">
               <table class="table table-bordered table-striped">
                   <thead>
                   <tr>
                       <th>User</th>
                       <th>Email</th>
                       <th>Proof</th>
                       <th>Action</th>
                       <th>Date Submitted</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($proofs as $proof)
                       <tr>
                           <td>{{ $proof->user->name }}</td>
                           <td>{{ $proof->user->email }}</td>
                           <td>
                               <a href="uploads/{{ $proof->proof }}">
                                   See Proof
                               </a>
                           </td>
                           <td>
                               @if ($proof->seen == 0)
                                   <a href="{{ url('/update-proof/' . $proof->id) }}" class="btn btn-xs btn-primary">Mark as seen</a>
                               @else
                                   <p>Marked as seen</p>
                               @endif
                           </td>
                           <td>{{ $proof->created_at }}</td>
                       </tr>
                   @endforeach
                   </tbody>
               </table>
               <div class="pagination">
                   {{ $proofs->links() }}
               </div>
           </div>
       </section>

        <section style="margin-top: 2%;">
            <h3 class="text-center">Credit a user wallet</h3>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ url('/credit-wallet') }}">
                @csrf
                <div class="form-group">
                    <label for="email">User Email</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <input type="number" class="form-control" name="amount" id="amount" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="Credit User" class="btn btn-primary">
                </div>
            </form>
        </section>
    </div>

@endsection
