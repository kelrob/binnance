<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'country', 'phone', 'plan', 'bank_name', 'account_name', 'account_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function payment_proof() {
        return $this->hasMany('App\PaymentProof');
    }

    public function withdrawal_request() {
        return $this->hasMany('App\WithdrawalRequest');
    }

    public function investment() {
        return $this->hasMany('App\Investment');
    }

    public function bank_details() {
        return $this->hasOne('App\BankDetail');
    }
}
