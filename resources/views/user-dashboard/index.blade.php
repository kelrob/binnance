@extends('layouts.auth-dashboard')

@section('content')
    <section class="breadcrumb-section"
             style="background-image: url('https://bitcryptopay.com/assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>User Dashboard</h5>

                    </div>
                    <!-- Breadcrumb section End -->

                </div>
            </div>
        </div>
    </section>
    <section class="trading-view">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript"
                    src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                const data = {
                    "symbols": [
                        {
                            "title": "S&P 500",
                            "proName": "OANDA:SPX500USD"
                        },
                        {
                            "title": "Shanghai Composite",
                            "proName": "INDEX:XLY0"
                        },
                        {
                            "title": "EUR/USD",
                            "proName": "FX_IDC:EURUSD"
                        },
                        {
                            "title": "BTC/USD",
                            "proName": "BITSTAMP:BTCUSD"
                        },
                        {
                            "title": "ETH/USD",
                            "proName": "BITSTAMP:ETHUSD"
                        }
                    ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "displayMode": "adaptive",
                    "locale": "en"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </section>

    <section class="account-information">
        <div class="content_padding">
            <div class="container user-dashboard-body" style="padding: 4% 2%; background-color: #eee; margin-top: 2%;">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 style="color: #fff"> Account Statistics</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-money fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge dashboard-balance-text"> $ <span
                                                        data-counter="counterup"
                                                        data-value="">{{ number_format(Auth::user()->wallet) }}</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Current Balance</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-recycle fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge dashboard-balance-text">
                                                    $ <span data-counter="counterup" data-value="0">{{ $totalROI }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Total ROI</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-cloud-download fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge dashboard-balance-text"> <span
                                                        data-counter="counterup" data-value="0">{{ $totalDeposit }}</span></div>

                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Deposits Count</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-cloud-upload fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge dashboard-balance-text">
                                                    $ <span data-counter="counterup" data-value="0">0</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left">Total Withdraws</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container">
                                <div class="tradingview-widget-container__widget"></div>
                                <div class="tradingview-widget-copyright"><a
                                        href="https://www.tradingview.com/markets/cryptocurrencies/prices-all/"
                                        rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                                <script type="text/javascript"
                                        src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js"
                                        async>
                                    {
                                        "width"
                                    :
                                        "100%",
                                            "height"
                                    :
                                        "700",
                                            "defaultColumn"
                                    :
                                        "moving_averages",
                                            "screener_type"
                                    :
                                        "crypto_mkt",
                                            "displayCurrency"
                                    :
                                        "USD",
                                            "colorTheme"
                                    :
                                        "dark",
                                            "locale"
                                    :
                                        "en"
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
