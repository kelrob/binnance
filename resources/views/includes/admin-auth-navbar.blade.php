<div class="col-md-12">
    <div class="admin-content">
        <div class="admin-text">


            <div id="ytWidget"></div>
            <script
                src="../translate.yandex.net/website-widget/v1/widget5672.js?widgetId=ytWidget&amp;pageLang=en&amp;widgetTheme=light&amp;autoMode=true"
                type="text/javascript"></script>
        </div>
    </div>
</div>
<nav class="main-menu wow" data-wow-duration="2s">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <a href="{{ url('/') }}"><img src="assets/images/logo/logo.jpeg" style="max-height:40px;"></a>
                </div>
            </div>
            <div class="col-md-9 text-right">
                @guest
                    <ul id="header-menu" class="header-navigation">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{ url('about') }}">About Us</a></li>
                        <li><a href="{{ url('faq') }}">Faq</a></li>
                        <li><a href="{{ url('contact') }}">Contact</a></li>
                        <li><a class="page-scroll" href="#">Account <i class="fa fa-angle-down"></i></a>
                            <ul class="mega-menu mega-menu1 mega-menu2 menu-postion-4">
                                <li class="mega-list mega-list1">
                                    <a class="page-scroll" href="{{ url('sign-in') }}">Login</a>
                                    <a class="page-scroll" href="{{ url('sign-up') }}">Register</a></li>
                            </ul>
                        </li>
                    </ul>
                @else
                    <ul id="header-menu" class="header-navigation">
                        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li><a>Settings <i class="fa fa-angle-down"></i></a>
                            <ul class="mega-menu mega-menu1 mega-menu2 menu-postion-2">
                                <li class="mega-list mega-list1">
                                    <a class="page-scroll" href="{{ url('account-info') }}">Account</a>
                                    <a class="page-scroll" href="{{ url('payment-proofs') }}">Payments</a>
                                    <a class="page-scroll" href="{{ url('withdrawal-requests') }}">Withdrawal Requests</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="page-scroll" href="#">Hi, {{ Auth::user()->username }} <i class="fa fa-caret-down"></i></a>
                            <ul class="mega-menu mega-menu1 mega-menu2 menu-postion-3">
                                <li class="mega-list mega-list1">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endguest
            </div>
        </div>

    </div>

</nav>
