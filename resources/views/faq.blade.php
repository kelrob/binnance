@extends('layouts.dashboard')

@section('content')
    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>FAQS Page</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>
    <!--faq page content start-->
    <section class="faq-section section-padding section-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- section header start -->
                    <div class="section-header">
                        <h3><span>General</span> FAQ</h3>
                        <p><img src="assets/images/logo/icon.png" alt="icon"></p>
                    </div>
                    <!-- section header end -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="faq">
                        <div class="container">
                            <div class="faq-content">
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane fade active in" id="domainsTab">
                                        <div class="panel-group accordion" id="accordion4" >
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ8" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> WHAT IS ROI? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ8" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> ROI means Return On Investment</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ7" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> CAN I OPEN SEVERAL ACCOUNTS ON BCP WEBSITE? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ7" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">No, every client of Binance Minning can have only one account for their investment and earnings.</span><br></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ6" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> HOW FAST IS MY MONEY CREDITED WHEN MAKING DEPOSIT? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ6" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">Making deposit by using Bitcoin or Gift card takes from 3 confirmations ( about 30 minutes ).</span><br></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ5" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> WHAT IS BCP INVESTMENT PLATFORM? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ5" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <h2 style="margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);"><span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;"> Binance Minning Investment Platform is an experienced team of dedicated brokers and analysts who involved in crypto trading.</span><br></h2></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ4" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> WHERE DO WE INVEST IN? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ4" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <h2 style="margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);"><span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">Our company is engaged in a full investment service focused on the Bitcoin markets.</span><br></h2></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ3" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> WHAT PAYMENT PROCESSORS ARE AVAILABLE FOR INVESTMENT? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ3" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">You can make deposits and withdraw through Bitcoin, Western Union and MoneyGram</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading" role="tab">

                                                    <h4 class="panel-title"> <a href="#domainsTabQ1" role="button" data-toggle="collapse" data-parent="#accordion4" aria-expanded="false" class="collapsed"> WHAT IS THE MINIMUM AMOUNT CAN I WITHDRAW FROM ACCOUNT BALANCE? <i class="fa fa-minus"></i> </a></h4>
                                                </div>
                                                <div id="domainsTabQ1" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                        <p> <span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">You can make withdrawal instantly to your account or wallets of any amount from $10,000 and above to $500,000.&nbsp;</span><span style="color: rgb(121, 121, 121); font-family: ralewayregular; font-size: 16px;">Withdrawals are instantly processed. No pending! No Stories..</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
