@extends('layouts.dashboard')

@section('content')

    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/logo/bb.png')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>About Page</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>

    <!--about us page content start-->
    <section class="section-padding about-us-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); margin-top: 0px; margin-right: 0px; margin-left: 0px; font-size: 24px; padding: 0px;"><br></h2><h2 style="font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); margin-top: 0px; margin-right: 0px; margin-left: 0px; font-size: 24px; padding: 0px;">Why Binance Minning?</h2><p style="margin-bottom: 15px; padding: 0px; text-align: justify;"><font color="#000000">Binance Minning is one of the leading Cryptocurrency Investment in the world, offering cryptocurrency mining capacities in every range - for newcomers, interested home miners, as well as large scale investors. Our mission is to make acquiring cryptocurrencies easy and fast for everyone.</font></p><p style="margin-bottom: 15px; padding: 0px; text-align: justify;"><font color="#000000"><br></font></p><p style="margin-bottom: 15px; padding: 0px; text-align: justify;"><font color="#000000">We provide a multi-algorithm, multi-coin cloud mining service using the latest technology - without any pool fees. The ultimate goal of our existence is to make cryptocurrency mining an easy, smart and rewarding experience for all. Our services already attracted more than 2.000.000 people - We’d be happy to serve you as well!</font></p><h2 style="font-family: DauphinPlain; line-height: 24px; color: rgb(0, 0, 0); margin-top: 0px; margin-right: 0px; margin-left: 0px; font-size: 24px; padding: 0px;">Who we are</h2><p style="margin-bottom: 15px; color: rgb(0, 0, 0); padding: 0px; text-align: justify;">The story of Genesis Mining started at the end of 2013. Our founders got to know each other by using the same platform for buying and selling Bitcoins. They were fascinated by the technology and wanted to build their own farm, only to realize all their friends wanted to participate as well.They came up with the idea of mining as a service and built the first mining farm in Eastern Europe. Since our founding, we have grown tremendously and a lot has happened, but one thing remains constant: We are all strong believers in the future of digital currencies and we love being part of this growing community.</p>
                </div>
            </div>
        </div>
    </section>
@endsection
