<?php

namespace App\Http\Controllers;

use App\AccountDetail;
use App\Investment;
use App\PaymentProof;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->account_type == 'admin') {
            return redirect(url('/admin-dashboard'));
        } else {
            $totalROI = DB::table('investments')->where('user_id', Auth::user()->id)->sum('roi');
            $totalDeposit = DB::table('payment_proofs')->where('user_id', Auth::user()->id)
                ->where('seen', 1)
                ->count();
            return view('user-dashboard.index', compact('totalROI', 'totalDeposit'));
        }
    }

    public function fundWallet()
    {
        $accountInfo = AccountDetail::find(1)->account_info;
        return view('user-dashboard.fund-wallet', compact('accountInfo'));
    }

    public function withdrawFunds() {
        return view('user-dashboard.withdraw-funds');
    }

    public function withdrawalHistory() {
        $userId = Auth::user()->id;

        $withdrawals = WithdrawalRequest::where('user_id', $userId)->orderBy('id', 'DESC')->get();
        return view('user-dashboard.withdrawal-history', compact('withdrawals'));
    }

    public function depositHistory() {
        $userId = Auth::user()->id;

        $deposits = PaymentProof::where('user_id', $userId)->orderBy('id', 'DESC')->get();
        return view('user-dashboard.deposit-history', compact('deposits'));
    }

    public function changePassword() {
        return view('user-dashboard.change-password');
    }

    public function investmentHistory() {
        $userId = Auth::user()->id;
        $histories = Investment::where('user_id', $userId)->get();

        return view('user-dashboard.investment-history', compact('histories'));
    }

    public function roiHistory() {

        $userId = Auth::user()->id;
        $histories = Investment::where('user_id', $userId)->get();

        return view('user-dashboard.roi-history', compact('histories'));
    }

    public function transactionLog() {
        $userId = Auth::user()->id;
        $withdrawals = WithdrawalRequest::where('user_id', $userId)->get();
        $deposits = PaymentProof::where('user_id', $userId)->get();

        return view('user-dashboard.transaction-log', compact('withdrawals', 'deposits'));
    }
}
