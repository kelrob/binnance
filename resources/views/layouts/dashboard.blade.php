<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Binance Minning | Revolutionary Money Making Platform - Home Page</title>
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href={{ url('assets/images/logo/icon.png') }}>
    <!--bootstrap Css-->
    <link href={{ url('assets/front/css/bootstrap.min.css') }} rel="stylesheet">
    <!--font-awesome Css-->
    <link href={{ url('assets/front/css/font-awesome.min.css') }} rel="stylesheet">
    <!-- Lightcase  Css-->
    <link href={{ url('assets/front/css/lightcase.css') }} rel="stylesheet">
    <!--WOW Css-->
    <link href={{ url('assets/front/css/animate.min.css') }} rel="stylesheet">
    <!--Slick Slider Css-->
    <link href={{ url('assets/front/css/slick.css') }} rel="stylesheet">
    <!--Slick Nav Css-->
    <link href={{ url('assets/front/css/slicknav.min.css') }} rel="stylesheet">
    <!--Swiper  Css-->
    <link href={{ url('assets/front/css/swiper.min.css') }} rel="stylesheet">
    <!--Style Css-->
    <link href={{ url('assets/front/css/style.css') }} rel="stylesheet">
    <!-- Theam Color Css-->
    <link href={{ url('assets/css/color82c6.css?color=0084D4') }} rel="stylesheet">
    <!--Responsive Css-->
    <link href={{ url('assets/front/css/responsive.css') }} rel="stylesheet">

    <link rel="stylesheet" href={{ url('assets/css/ion.rangeSlider.css') }}>
    <link rel="stylesheet" href={{ url('assets/css/ranger-style.css') }}>
    <link rel="stylesheet" href={{ url('assets/css/ion.rangeSlider.skinFlat.css') }}>
    <style>
        .price-table {
            margin-bottom: 45px;
    </style>
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>

    <link rel="stylesheet" href={{ url('assets/front/2/css/style.css') }}>

    <script src={{ url('assets/front/2/js/modernizr.js') }}></script>

    <link rel="stylesheet" type="text/css" href={{ url('../cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css') }}>

    <script src={{ url('../cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js') }}></script>
    <script src={{ url('assets/js/countries.js') }}></script>
</head>

<body data-spy="scroll">
<!-- Start Pre-Loader-->
<div id="preloader">
    <div data-loader="circle-side"></div>
</div>
<!-- End Preload -->
@include('includes.top-header')

<!--support bar  top end-->

<!--main menu section start-->
@include('includes.navbar')
<!--main menu section end-->

@yield('content')

<div class="clearfix"></div>


<div class="clearfix"></div>

<!--payment method section start-->
<section class="client-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header wow zoomInDown" data-wow-duration="2s">
                    <h2><span>PAYMENT METHOD </span> WE ACCEPT</h2>
                    <p><img src={{ url('assets/images/logo/icon.png') }} alt="icon"></p>
                </div><!-- section-heading -->
                <div class="section-wrapper">
                    <div class="client-list">
                        <!-- Swiper -->
                        <div class="swiper-container client-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/1508826560h4.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/1508740770h6.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/150882822860h8.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/5daf37a4e13c9.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/1508740692h7.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/1587290997h7.png') }}
                                                alt="client"></a></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="our-client wow rotateIn" data-wow-duration="2s"><a href="#"><img
                                                class="img-responsive" src={{ url('assets/images/1587303051h7.png') }}
                                                alt="client"></a></div>
                                </div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next">
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            </div>
                            <div class="swiper-button-prev">
                                <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                            </div>
                        </div><!-- client container -->
                    </div><!-- client list-->
                </div><!-- swiper wrapper -->
            </div>

        </div><!-- row -->
    </div><!-- container -->
</section>
<!--end payment method section start-->
<!--footer area start-->
<footer id="contact" class="footer-area">
    <!--footer area start-->
    <div class="footer-bottom">
        <div class="footer-support-bar">
            <!-- Footer Support List Start -->
            <div class="footer-support-list">
                <ul>
                    <li class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="1s">
                        <div class="footer-thumb"><i class="fa fa-headphones"></i></div>
                        <div class="footer-content">
                            <p>24/7 Customer Support</p>
                        </div>
                    </li>
                    <li class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="2s">
                        <div class="footer-thumb"><i class="fa fa-envelope"></i></div>
                        <div class="footer-content">
                            <p><a href="contact.html">support@binanceminning.com</a></p>
                        </div>
                    </li>
                    <li class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="3s">
                        <div class="footer-thumb"><i class="fa fa-comments-o"></i></div>
                        <div class="footer-content">
                            <p>Friendly Support Ticket</p>
                        </div>
                    </li>
                    <li class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="4s">
                        <div class="footer-thumb"><i class="fa fa-phone"></i></div>
                        <div class="footer-content">
                            <p>+1(224) 698-7620</p>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- Footer Support End -->
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 wow fadeInLeft" data-wow-duration="3s">
                    <p class="copyright-text">

                    </p>
                </div>
                <div class="col-md-4 col-sm-9 wow bounceInDown" data-wow-duration="3s">
                    <p class="copyright-text">

                        Copyright &copy; Binance Minning 2020. All Right Reserved || <a href="https://abioliansolutions.com.ng" target="_blank">Powered By: Abiolian Solutions Enterprise</a>
                    </p>
                </div>
                <div class="col-md-4 col-sm-3 wow fadeInRight" data-wow-duration="3s">

                </div>
            </div>
        </div>
    </div>
    <div id="back-to-top" class="scroll-top back-to-top" data-original-title="" title="">
        <i class="fa fa-angle-up"></i>
    </div>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5e9db7ef35bcbb0c9ab2f01b/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</footer>
<style type="text/css">
    li.export-main {
        visibility: hidden;
    }
</style>
<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<!--jquery script load-->
<script src="assets/front/js/jquery.js"></script>

<script src="assets/front/js/bootstrap.min.js"></script>
<!-- Gmap Load Here -->
<script src="assets/front/js/gmaps.js"></script>
<!-- Map Js File Load -->
<script src="assets/front/js/map-script82c6.php?color=0084D4"></script>
<!-- Highlight script load-->
<script src="assets/front/js/highlight.min.js"></script>
<!--Jquery Ui Slider script load-->
<script src="assets/front/js/jquery-ui-slider.min.js"></script>
<!--Circleful Js File Load-->
<script src="assets/front/js/jquery.circliful.js"></script>
<!--CounterUp script load-->
<script src="assets/front/js/jquery.counterup.min.js"></script>
<!-- Ripples  script load-->
<script src="assets/front/js/jquery.ripples-min.js"></script>
<!--Slick Nav Js File Load-->
<script src="assets/front/js/jquery.slicknav.min.js"></script>
<!--Lightcase Js File Load-->
<script src="assets/front/js/lightcase.js"></script>
<!--particle Js File Load-->
<script src="assets/front/js/particles.min.js"></script>
<!--particle custom Js File Load-->
<script src="assets/front/js/particles-custom.js"></script>
<!--RainDrops script load-->
<script src="assets/front/js/raindrops.js"></script>
<!--Easing script load-->
<script src="assets/front/js/easing-min.js"></script>
<!--Slick Slider Js File Load-->
<script src="assets/front/js/slick.min.js"></script>
<!--Swiper script load-->
<script src="assets/front/js/swiper.min.js"></script>
<!--WOW script load-->
<script src="assets/front/js/wow.min.js"></script>
<!--WayPoints script load-->
<script src="assets/front/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/ion.rangeSlider.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true
        });
        wow.init();
    });
</script>
<script>
    $.each($('.slider-input'), function () {
        var $t = $(this),

            from = $t.data('from'),
            to = $t.data('to'),

            $dailyProfit = $($t.data('dailyprofit')),
            $totalProfit = $($t.data('totalprofit')),

            $val = $($t.data('valuetag')),

            perDay = $t.data('perday'),
            perYear = $t.data('peryear');


        $t.ionRangeSlider({
            input_values_separator: ";",
            prefix: '$ ',
            hide_min_max: true,
            force_edges: true,
            onChange: function (val) {
                $val.val('$ ' + val.from);

                var profit = (val.from * perDay / 100).toFixed(1);
                profit = '$ ' + profit.replace('.', '.');
                $dailyProfit.text(profit);

                profit = ((val.from * perDay / 100) * perYear).toFixed(1);
                profit = '$ ' + profit.replace('.', '.');
                $totalProfit.text(profit);

            }
        });
    });
    $('.invest-type__profit--val').on('change', function (e) {

        var slider = $($(this).data('slider')).data("ionRangeSlider");

        slider.update({
            from: $(this).val().replace('$ ', "")
        });
    })
</script>
<!--Main js file load-->
<script src={{ url('assets/front/js/main.js') }}></script>
<script language="javascript">
    populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
    populateCountries("country2");
    populateCountries("country2");
</script>
<script src={{ url('assets/front/2/js/main.js') }}></script>
<!-- <script src="https://bitcryptopay.com/assets/js/main.js"></script> -->
<!--swal alert message-->

<!--end swal alert message-->
<script>
    var mobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

    hljs.initHighlightingOnLoad();
    hljs.configure({useBR: true});
    jQuery('#raindrops').raindrops({color: '#fff', canvasHeight: 5});
    jQuery('#raindrops-green').raindrops({color: '#0084D4 ', canvasHeight: 5});

</script>

</body>
</html>
