<div class="animation-element">
    <!-- End Pre-Loader -->
    <!--support bar  top start-->
    <div class="support-bar-top wow slideInLeft" data-wow-duration="2s" id="raindrops-green">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-info">
                        <a href="mailto:support@bitcryptopay.com"> <i class="fa fa-envelope email"
                                                                      aria-hidden="true"></i>
                            support@bitcryptopay.com</a>
                        <a href="#"> <i class="fa fa-phone" aria-hidden="true"></i> +447506877389 </a>
                    </div>
                </div>
                <div class="col-md-6 text-right bounceIn">
                    <div class="contact-admin">
                        <a href="{{ url('/sign-in') }}"><i class="fa fa-user"></i> LOGIN </a>
                        <a href="{{ url('/sign-up') }}"><i class="fa fa-user-plus"></i> REGISTER</a>
                        <div class="support-bar-social-links">
                            <a href="https://tawk.to/chat/5d3e3ad16d808312283a6f10/default"><i class="fa fa-comments-o"
                                                                                               aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
