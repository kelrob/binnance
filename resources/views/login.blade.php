@extends('layouts.dashboard')

@section('content')

    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/bg.html')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>Log In</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>
    <!--Header section end-->


    <!--login section start-->
    <section  class="circle-section section-padding section-background">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-md-offset-3">
                    <div class="login-admin login-admin1">
                        <div class="login-header text-center">
                            <h6>Login Form</h6>
                        </div>


                        <div class="login-form">
                            @if (count($errors->all()) > 0)
                                <div class="alert alert-danger" align="center">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br/>
                                    @endforeach
                                </div>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                @csrf

                                <input type="text" name="username" id="username" required placeholder="Enter your User Name"/>
                                <input type="password" name="password" id="password" required placeholder="Enter your Password"/>

                                <input value="Login" type="submit">

                                <div class="form-group">
                                    <div class="cols-sm-10 cols-sm-offset-2">
                                        <div class="col-sm-12 text-center">
                                            <a class="btn btn-link" href="{{ url('password-reset') }}">
                                                Forgot Your Password?
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
