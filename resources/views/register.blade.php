@extends('layouts.dashboard')

@section('content')
    <!--header section start-->
    <section class="breadcrumb-section" style="background-image: url('assets/images/bg.html')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- breadcrumb Section Start -->
                    <div class="breadcrumb-content">
                        <h5>Register</h5>
                    </div>
                    <!-- Breadcrumb section End -->
                </div>
            </div>
        </div>
    </section>
    <!--Header section end-->

    <!--login section start-->
    <div class="login-section section-padding login-bg">
        <div class="container">

            <div class="login-admin login-admin1">
                @if (count($errors->all()) > 0)
                    <div class="alert alert-danger" align="center">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br/>
                        @endforeach
                    </div>
                @endif
                <div class="login-header text-center">
                    <h6>Register Form</h6>
                </div>
                <div class="login-form">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="referral" id="referral" value="{{ old('referral') }}" placeholder="Reference ID - Ignore if none"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text"  name="username" id="username" value="{{ old('username') }}" required placeholder="Enter your Username"/>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text"  name="name" id="name" value="{{ old('name') }}" required placeholder="Enter your Name"/>
                            </div>
                            <div class="col-md-12">
                                <select required id="country" name="country"></select>
                            </div>


                            <div class="col-md-6">
                                <input type="text"  name="email" id="email" value="{{ old('email') }}" required placeholder="Enter your Email"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text"  name="phone" id="phone" value="{{ old('phone') }}" required placeholder="Enter your Phone Number"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="password"  name="password" id="password" required placeholder="Enter your Password"/>
                            </div>

                            <div class="col-md-6">
                                <input type="password"  name="password_confirmation" id="password_confirmation" required placeholder="Confirm your Password"/>
                            </div>
                            {{-- <div class="col-md-12">
                                <label>Please select an Investment Plan </label>
                                <select required class="form-control" name="plan">

                                    <option value="">Select A Plan</option>
                                    <option value="plan">Starter</option>
                                    <option value="plan">Silver</option>
                                    <option value="plan">Gold</option>
                                    <option value="plan">Platinum</option>
                                    USD

                                </select>
                            </div> --}}
                        </div>
                        <hr>
                        {{-- <br>
                        <div class="row">
                            <div class="col-md-12"><h3 class="text-center">BANK DETAILS</h3></div>
                            <div class="col-md-6">
                                <input type="text"  name="bank_name" id="bank_name" required placeholder="Enter your Bank Name"/>
                            </div>

                            <div class="col-md-6">
                                <input type="text"  name="account_name" id="account_name" required placeholder="Enter your Account Name"/>
                            </div>

                            <div class="col-md-6">
                                <input type="text"  name="account_number" id="account_number" required placeholder="Enter your Account Number"/>
                            </div>
                        </div>
                        <br> --}}
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <input value="Register" type="submit">
                            </div>
                        </div>


                    </form>
                </div>
                <div class="text-center" style="text-transform: uppercase;">
                    <br><br>
                    <a href="{{ url('password-reset') }}">Forgot Password</a> | <a href="{{ url('sign-in') }}">Login</a>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
    <!--login section end-->

@endsection
