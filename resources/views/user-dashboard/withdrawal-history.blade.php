@extends('layouts.auth-dashboard')

@section('content')
    <div class="container">
        <section class="fund-wallet">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 3%;">
                        <h3 class="text-center">Withdrawal History</h3>
                        <hr style="width: 10%"/>
                    </div>
                    <div class="col-lg-12" style="margin-top: 3%">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($withdrawals as $withdrawal)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$withdrawal->amount}}</td>
                                        <td>
                                            @if($withdrawal->attended_to == 0)
                                                Not attended to
                                            @else
                                                Attended to
                                            @endif
                                        </td>
                                        <td>{{$withdrawal->created_at->format('d M Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
