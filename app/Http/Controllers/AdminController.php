<?php

namespace App\Http\Controllers;

use App\AccountDetail;
use App\PaymentProof;
use App\User;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $users = User::with('investment')->get();
        return view('admin-dashboard.index', compact('users'));
    }

    public function settings() {
        $accountInfo = AccountDetail::find(1)->account_info;
        return view('admin-dashboard.settings', compact('accountInfo'));
    }

    public function updateSettings(Request $request) {
        $accountInfo = $request->account_info;

        $settings = AccountDetail::where('id',1)->first();
        $settings->account_info = ($accountInfo);
        $settings->save();

        return Redirect::back()->with('message', 'Account Details Updated Successfully');
    }

    public function postPaymentProof(Request $request) {
        $request->validate([
            'file' => 'required',
        ]);

        $fileName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);

        $paymentProof = new PaymentProof();
        $paymentProof->amount = $request->amount;
        $paymentProof->user_id = Auth::user()->id;
        $paymentProof->proof = $fileName;
        $paymentProof->save();

        return back()
            ->with('success','You have successfully upload file.')
            ->with('file',$fileName);
    }

    public function allPayments() {
        $proofs = PaymentProof::with('user')->orderBy('id','DESC')->paginate(10);;

        return view('admin-dashboard.payment-proofs', compact('proofs'));
    }

    public function updateProof($id) {
        $proof = PaymentProof::find($id);
        $proof->seen = 1;
        $proof->save();

        return back();
    }

    public function updateWithdrawal($id) {
        $withdrawal = WithdrawalRequest::find($id);

        $user  = User::find($withdrawal->user_id);
        $user->wallet = $user->wallet - $withdrawal->amount;
        $user->save();

        $withdrawal->attended_to = 1;
        $withdrawal->save();

        return back();
    }

    public function creditWallet(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'amount' => 'required|integer'
        ]);

        $userExist = User::where('email', $request->email)->count();

        if ($userExist > 0) {
            $user = User::where('email', $request->email)->first();
            $user->wallet = $user->wallet + $request->amount;
            $user->save();

            return back()->with('success','User Wallet Credited Successfully.');
        } else {
            return back()->with('error','User Does not exist.');
        }
    }

    public function withdrawalRequest() {
        $withdrawals = WithdrawalRequest::with('user')
            ->orderBy('id', 'DESC')
            ->paginate(10);;

        return view('admin-dashboard.withdrawal-requests', compact('withdrawals'));
    }

    public function deleteUserAccount($id) {
        $user = User::find($id);
        $user->delete();

        return redirect()->back()->with('success', 'Account Deleted Successfully');
    }
}
