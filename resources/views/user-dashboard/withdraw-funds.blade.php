@extends('layouts.auth-dashboard')

@section('content')
    <div class="container">
        <section class="fund-wallet">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 3%;">
                        <h3 class="text-center">Withdraw funds</h3>
                        <hr style="width: 10%" />
                    </div>
                    <div class="col-lg-12" style="margin-top: 3%">
                        <form method="post" action="{{ url('process-withdrawal') }}" style="padding: 1%; border: 1px solid #eee;">
                            @csrf
                            @if($errors->any())
                                <div class="alert alert-danger">{{$errors->first()}}</div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    {!! \Session::get('success') !!}
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="number" name="amount" class="form-control" id="amount"/>
                            </div>
                            <div class="form-group">
                                <label>Where will you like to receive your withdrawal</label>
                                <select class="form-control" name="withdrawl_mode" required>
                                    <option value="bank">Bank Account</option>
                                    <option value="bitcoin">Bitcoin Wallet</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>If Bitcoin wallet. Please specify Bitcoin wallet ID</label>
                                <input type="text" name="bitcoin_wallet" class="form-control" />
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Withdraw" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        const toggleProofForm = () => {
            $('#toggleForm').fadeToggle();
        }
    </script>
@endsection
