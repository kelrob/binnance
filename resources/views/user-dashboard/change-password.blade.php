@extends('layouts.auth-dashboard')

@section('content')
    <div class="container">
        <section class="fund-wallet">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12" style="margin-top: 3%;">
                        <h3 class="text-center">Change Password</h3>
                        <hr style="width: 10%"/>
                    </div>
                    <div class="col-lg-12" style="margin-top: 3%">
                        <form method="post" action="{{ url('change-password') }}" style="padding: 1%; border: 1px solid #eee;">
                            @csrf
                            @if($errors->any())
                                <div class="alert alert-danger">{{$errors->first()}}</div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    {!! \Session::get('success') !!}
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="old-password">Old Password</label>
                                <input type="password" name="oldPassword" class="form-control" id="old-password">
                            </div>
                            <div class="form-group">
                                <label for="new-password">New Password</label>
                                <input type="password" name="newPassword" class="form-control" id="new-password">
                            </div>
                            <div class="form-group">
                                <label for="new-password-again">Confirm New Password</label>
                                <input type="password" name="newPasswordAgain" class="form-control" id="new-password-again">
                            </div>
                            <div class="form-group">
                                <input type="submit"  class="btn btn-primary" value="Change Password">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
